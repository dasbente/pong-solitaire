extends Sprite

signal game_over
signal score_point

export var speed = 1.0
export var multiplier = 1.1

var direction = Vector2(0, 0)
var racket
var passed = false

func _ready():
	racket = get_parent()
	go()

func go():
	# random start angle
	direction = Vector2.UP.rotated(rand_range(0.0, 2.0 * PI))

func reset():
	position = Vector2(0, 0)
	direction = Vector2(0, 0)

func _physics_process(delta):
	position += direction * speed
	
	if (!get_viewport_rect().has_point(global_position)):
		emit_signal("game_over")
		reset()
		return
	
	if !passed and racket.check_passed(position):
		if racket.check_collision(position):
			position = racket.fix(position)
			direction = racket.calc_bounce(position)
			speed *= multiplier
			emit_signal("score_point")
		else:
			passed = true
