extends Node2D

export var distance = 0.8
export var racket_size = 10
export var min_racket_size = 5
export var shrink_threshold = 10
export var shrink_multiplier = 0.8

func _ready():
	get_tree().get_root().connect("size_changed", self, "init")
	init()

func _physics_process(delta):
	update_sprite()

func init():
	position.x = get_viewport_rect().size.x / 2
	position.y = get_viewport_rect().size.y / 2
	update_sprite()

func get_racket_distance():
	return distance * (get_viewport_rect().size.y / 2)

func update_sprite():
	var dist = get_racket_distance()
	var mouse_pos = get_local_mouse_position().normalized()
	
	$Racket.position = mouse_pos * dist
	$RacketEndLeft.position = $Racket.position.rotated(-(racket_size / 360.0) * 2 * PI)
	$RacketEndRight.position = $Racket.position.rotated(racket_size / 360.0 * 2 * PI)

# check if position is outside of racket range
func check_passed(pos : Vector2):
	return pos.length() > get_racket_distance()

func collision_angle(pos):
	var angle = ($Racket.position).angle_to(pos)
	var angle_range = (racket_size / 360.0 * 2 * PI)
	
	return  angle / angle_range

# check if position would have collided with racket
func check_collision(pos):
	return check_passed(pos) and abs(collision_angle(pos)) < 1

# project pos onto racket
func fix(pos):
	return pos.normalized() * get_racket_distance()

# calculate bounce based on where racket was hit
func calc_bounce(pos):
	var diversion_angle = collision_angle(pos) * PI / 4
	return -$Racket.position.normalized().rotated(diversion_angle)


var score = 0

func _on_Ball_score_point():
	score += 1
	
	if score % shrink_threshold == 0:
		racket_size = max(min_racket_size, racket_size * shrink_multiplier)
